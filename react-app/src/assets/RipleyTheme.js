export const RipleyTheme = {
  palette: {
    typography: {
      fontFamily: 'Roboto',
      useNextVariants: true,
    },
    primary: {
      light: '#75d9ad',
      main: '#b30d2d',
      dark: '#007941',
      contrastText: '#ffffff',
    },
    secondary: {
     main: '#e75353',
   },
    title: {
      light: '#75d9ad',
      main: '#6a6a6a',
      dark: '#007941',
      contrastText: '#ffffff',
    },
  },
  classes:{
    fontbold: {
      fontWeight: 'bold',
    },
  }
};

export default RipleyTheme;
