import Model from './Model';
/*
 * Product Model Class
 */
class ProductModel extends Model {
  constructor(globals) {
    super();
    this.baseUrl = globals.baseUrl;
    this.headers = globals.headers;
    this.headers = '';
  }
  /**
  Details: Get Product by Id
  Params: id
  Return: {}
  */
  getProduct = async (id) => {
    let url = `${this.baseUrl}/product/${id}`;
    let response = {data:{ data: {}, error: true}};
    try {
      response = await this.get(url, this.headers);
    } catch (e) {
      console.log(e);
    }
    return response.data.data;
  };
  /**
  Details: Get Set of Products 
  Params: null
  Return: []
  */
  getSetProducts = async () =>{
    let url = `${this.baseUrl}/products`;
    let response = {data: { data: [], error: true }};
    try {
      response = await this.get(url, this.headers);
    } catch (e) {
      console.log(e);
    }
    return response.data.data;
  };
} //End class

export default ProductModel;
