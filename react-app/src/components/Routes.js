//React Component
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
//App Component
import ProductCatalog from './catalog/Index';
import SingleProduct from './product/Index';
//Style theme
import { RipleyTheme } from '../assets/RipleyTheme';
const theme = createMuiTheme(RipleyTheme);
/**
 * Create Routes
 */
function Routes(props) {
  return (
    <MuiThemeProvider theme={theme}>
      <BrowserRouter>
        <>
          <Switch>
            <Route path="/" component={ProductCatalog} exact />
            <Route path="/product/:id" component={SingleProduct} />
          </Switch>
        </>
      </BrowserRouter>
    </MuiThemeProvider>
  );
}

export default Routes;
