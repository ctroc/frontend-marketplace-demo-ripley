//React components:
import React from 'react';
import { connect } from 'react-redux';
import { updateLoader } from '../../redux/actions/globalActions';
//Material components:
import withWidth from '@material-ui/core/withWidth';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Divider from '@material-ui/core/Divider';
//App components
import SimpleAppBar from '../commons/NavBar';
//App Models:
import ProductModel from '../../models/ProductModel';
//App Styles:
import styles from './views/css/styles';

class SingleProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {},
    };
    this.model = new ProductModel(this.props.globals);
  }

  componentDidMount = async () => {
    const { match: { params } } = this.props;
    this.props.updateLoader(false); //Show Loader
    this.setState({ product: await this.model.getProduct(params.id) },
      ()=>{
        this.props.updateLoader(false); //Hide Loader
      }
    );
  };

  render(){
    let columns = 1;
    if(this.props.width==='xs'){ columns = 4; }
    const { classes } = this.props;
    return(
      <>
       <SimpleAppBar/>
       { this.state.product.name ? (
       <Grid container
             className={classes.root} spacing={3}
             direction="row"
             justify="space-around"
             alignItems="flex-start" >
        <Grid item sm={1} xs={12}>
          <GridList cellHeight={100} cols={columns}  >
            {this.state.product.images.map((image, index) => (
            <GridListTile key={index} >
              <img  src={`https://${image.substr(2)}`} width="100%" alt="" />
            </GridListTile>
              ))}
          </GridList>
        </Grid>
        <Grid item sm={5}>
          <img  src={`https://${this.state.product.fullImage.substr(2)}`} width="100%" alt="" />
        </Grid>
        <Grid item sm={5}>
          <List disablePadding={true} dense={true} >
            <ListItem>
              <Typography variant="h5">
                 {this.state.product.name}
              </Typography>
            </ListItem>

            <ListItem>
              <Typography variant="body2" color="textSecondary" >
                 sku: {this.state.product.partNumber}
              </Typography>
            </ListItem>

            <ListItem variant="body2" color="textSecondary">
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M9 11.3l3.71 2.7-1.42-4.36L15 7h-4.55L9 2.5 7.55 7H3l3.71 2.64L5.29 14z"/></svg>
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M9 11.3l3.71 2.7-1.42-4.36L15 7h-4.55L9 2.5 7.55 7H3l3.71 2.64L5.29 14z"/></svg>
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M9 11.3l3.71 2.7-1.42-4.36L15 7h-4.55L9 2.5 7.55 7H3l3.71 2.64L5.29 14z"/></svg>
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M9 11.3l3.71 2.7-1.42-4.36L15 7h-4.55L9 2.5 7.55 7H3l3.71 2.64L5.29 14z"/></svg>
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M9 11.3l3.71 2.7-1.42-4.36L15 7h-4.55L9 2.5 7.55 7H3l3.71 2.64L5.29 14z"/></svg>
            </ListItem>

            <ListItem>
              <Typography variant="body2" color="textSecondary" className={classes.spacing} >
                Normal {this.state.product.prices.formattedListPrice}
              </Typography>
            </ListItem>

            <ListItem>
              <Typography variant="body2" color="textSecondary" className={`${classes.spacing} ${classes.red} ${classes.bold}` } >
                Internet {this.state.product.prices.formattedListPrice}
              </Typography>
            </ListItem>
            { this.state.product.prices.discountPercentage ? (
            <ListItem>
              <Typography component="div" variant="body2" color="textSecondary" className={classes.spacing} >
                Descuento <Chip color="secondary" size="small" label={`${this.state.product.prices.discountPercentage}%`} />
              </Typography>
            </ListItem>
              ) : null }
          </List>
          <Divider/>
        </Grid>
      </Grid>
    ) : null }
    </>
    );
  }
}

const mapStateToProps = (state) => ({
  globals: state.globals,
});

const mapDispatchToProps = (dispatch) => {
  return { updateLoader: (show) => dispatch(updateLoader(show)) };
};

const wrapper = connect(mapStateToProps,mapDispatchToProps);
export default wrapper(withStyles(styles,{withTheme:true})(withWidth()(SingleProduct)));
