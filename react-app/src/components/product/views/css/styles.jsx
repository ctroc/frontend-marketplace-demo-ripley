const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    margin:'auto',
    marginTop: '5em',
    width: '90%',
  },
  leftBox:{
    height: '100px'
  },
  spacing:{
    wordSpacing: '22em',
    '@media (max-width: 780px)' : {
      wordSpacing: '8em',
    },
  },
  red:{
    color: '#e75353',
  },
  backgroundRed: {
    backgroundColor: '#e75353',
  },
  bold: {
    fontWeight: 'bold',
  },

}
export default styles;
