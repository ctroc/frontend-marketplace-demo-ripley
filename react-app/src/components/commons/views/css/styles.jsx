const styles = {
  root: {
    flexGrow: 1,
  },
  bar:{
    height: '55px',
    padding:'0',
  },
  title:{
    textDecoration: 'none',
    marginTop: '-8px !important',
    marginLeft: '3px',
    '@media (max-width: 780px)' : {
      marginTop: '-3px !important',
    },
  },
  toolbar:{
    marginLeft: '10%',
    '@media (max-width: 780px)' : {
        margin: 'auto',
    },
  },
}
export default styles;
