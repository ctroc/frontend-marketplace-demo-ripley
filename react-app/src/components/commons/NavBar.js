//React Component
import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
//Material Component
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import LinearLoader from './Loader';
//App Styles:
import styles from './views/css/styles';
//Resources
const logo = require('../../assets/images/logoRipley.png');

function SimpleAppBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="fixed" color="primary" className={classes.bar}>
        <Toolbar className={classes.toolbar}>
          <Typography variant="body1" color="inherit" className={classes.title} component={Link} to="/">
              <img width={15} src={logo} alt="Ripley" className={classes.logo} />
              &nbsp;Hot Price
          </Typography>
        </Toolbar>
        {props.globals.loader && <LinearLoader />}
      </AppBar>
    </div>
  );
}

const mapStateToProps = (state) => ({ globals: state.globals, });
const wrapper = connect(mapStateToProps,);

export default wrapper(withStyles(styles,{withTheme:true})(SimpleAppBar));
