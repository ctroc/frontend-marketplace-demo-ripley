const styles = {
  root: {
    flexGrow: 1,
    marginTop: '4em',
    padding:'1em',
  },
  borderBox: {
    border: 'solid 1px #ccc',
  },
  card: {
    maxWidth: 345,
    textDecoration: 'none',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  discount: {
    width: '100%',
    top: '-5px',
    borderRadius: '3px',
    backgroundColor: '#e75353',
    textAlign: 'center',
    lineHeight: '30px',
    fontSize: '14px',
    fontWeight: '500',
    color: '#fff',
    '@media (max-width: 780px)' : {
      width: '40px',
      height: '25px',
      lineHeight: '25px',
      fontSize: '12px',
    },
  },
  title:{
    paddingBottom: '5px',
    fontSize: 14,
    color: '#6a6a6a !important',
  },
  subTitle:{
    paddingTop: '10px',
    textTransform: 'uppercase',
  },
  price:{
    fontSize: '16px',
    color: '#e75353',
    fontweight: '600',
  }
};

export default styles;
