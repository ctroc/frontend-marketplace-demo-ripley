//React components:
import React from 'react';
import { connect } from 'react-redux';
import { updateLoader } from '../../redux/actions/globalActions';
import { Link } from 'react-router-dom';
//Material components:
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
//App components
import SimpleAppBar from '../commons/NavBar';
//App Models:
import ProductModel from '../../models/ProductModel';
//App Styles:
import styles from './views/css/styles';
//App Utils:
import { currencyFormatter } from '../../utils/Index';

class ProductCatalog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };

    this.model = new ProductModel(this.props.globals);
  }

  componentDidMount = async () => {
    this.props.updateLoader(true); //Show Loader
    this.setState({ products: await this.model.getSetProducts() },
      ()=>{
        this.props.updateLoader(false) //Hide Loader
      }
    );
  };

  render(){
    const { classes } = this.props;
    return(
      <div>
       <SimpleAppBar/>
       <Grid container className={classes.root} spacing={3} direction="row" justify="space-around" alignItems="center" >
        { this.state.products.length ? (
         <Grid item xs={12} sm={12} >
           <Typography className={ classes.title } >
             { this.state.products.length } Resultados
           </Typography>
           <Divider />
         </Grid>) : "" }
         {this.state.products.map((product, index) => (
           <Grid item xs={6} sm={3} key={index}>
             <Card className={classes.card} raised={true} elevation={0} component={Link} to={`/product/${product.partNumber}`} >
                <CardContent>
                  <Grid container
                        direction="row"
                        justify="flex-start"
                        alignItems="flex-start"
                    >
                    <Grid item xs={9} sm={9}>
                      <img src= {`https://${product.fullImage.substr(2)}`} width="100%" alt={product.name} />
                    </Grid>
                    { product.prices.discountPercentage ? (
                    <Grid item xs={3} sm={3} >
                      <div className={classes.discount} >
                        -{ product.prices.discountPercentage }%
                      </div>
                    </Grid>
                  ) : null }
                  </Grid>
                  <Typography variant="subtitle2" component="p" className={classes.subTitle}>
                    {product.name}
                  </Typography>
                 </CardContent>
                 <CardActions disableSpacing>
                   <Typography variant="subtitle2" className={classes.price}>
                     { currencyFormatter(product.prices.offerPrice)}
                   </Typography>
                 </CardActions>
               </Card>
           </Grid>
          ))}
        </Grid>
       </div>
    );
  }
}

const mapStateToProps = (state) => ({
  globals: state.globals,
});

const mapDispatchToProps = (dispatch) => {
  return { updateLoader: (show) => dispatch(updateLoader(show)) };
};

const wrapper = connect(mapStateToProps,mapDispatchToProps);
export default wrapper(withStyles(styles,{withTheme:true})(ProductCatalog));
